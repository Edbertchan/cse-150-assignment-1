@echo off
set /p test="Enter TestFile Name: "
set /p out="Enter OutputFile Name: "

echo Problem1: Board Correctness > %out%
py assignment1_p1.py < %test% >> %out%
echo. >> %out%
echo Problem2: Breadth First Search >> %out%
py assignment1_p2.py < %test% >> %out%
echo. >> %out%
echo Problem3: Depth First Search >> %out%
py assignment1_p3.py < %test% >> %out%
echo. >> %out%
echo Problem4: Iterative Deepening Search >> %out%
py assignment1_p4.py < %test% >> %out%
echo. >> %out%
echo Problem5: A* Search (Manhattan Heuristic) >> %out%
py assignment1_p5.py < %test% >> %out%
pause
