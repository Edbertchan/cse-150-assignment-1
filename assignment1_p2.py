#breadth first

#each state, we will move the 0 either up, down, left, or right
#for each, we check if it is possible. if it is, we will add that state to our queue
#else we dont

#if we end our queue without finding it, we return unsolvable. otherwise we solved it

# assignment1_p1.py

__authors__ = "golaes@ucsd.edu, bhaddad@ucsd.edu, eyc010@ucsd.edu"

import time
import sys
import Queue
from copy import deepcopy

# Start of Board Class definition: Represents the board in the game
class Board():
    board = [] # Representation of the Board
    empty = {} # Represents the point on the board with the empty space

    steps_til_state = "";
    movesOptions = ["UP", "DOWN","LEFT", "RIGHT"]
    def __init__(self, board, empty, steps_til_state):
        self.board = board
        self.empty = empty
        self.steps_til_state = steps_til_state

    # Sets printing behaviour of the Board
    def __str__(self):
        string = str(self.board[0])
        for y in range(1, len(self.board)):
            string = string + "\n" + str(self.board[y])
        return string

    def __deepcopy__(self, memo):
        return Board(deepcopy(self.board), deepcopy(self.empty), deepcopy(self.steps_til_state))

    # Verifies the board and returns True if the board is correct.
    # Returns False if the board is not correct
    def verify(self):
        count = 0
        size = len(self.board)*len(self.board[0])

        for y in range(0, len(self.board)):
            for x in range(0, len(self.board[y])):
                if self.board[y][x] != count:
                    return False
                count += 1

        return (count == size)

    # Takes a possible move ("UP", "DOWN", "LEFT", "RIGHT") and
    # returns True if that move is possible with
    # the given configuration
    def movePossible(self, move):
        if move == "UP":
            return self.empty["y"] != 0
        elif move == "DOWN":
            return self.empty["y"] != (len(self.board)-1)
        elif move == "LEFT":
            return self.empty["x"] != 0
        elif move == "RIGHT":
            return self.empty["x"] != (len(self.board[0])-1)
        else:
            return False
 # Returns True if a board created by the given move has
    # been visited before (as determined by giving a list of
    # previously visited boards
    def moveRevisited(self, move, boards_visited):       
        temp = deepcopy(self)
        temp.makeMove(move)

        return boards_visited.has_key(str(temp))
        #return len([b for b in boards_visited if temp == b]) > 0

    # Takes a move ("UP", "DOWN", "LEFT", "RIGHT") and
    # changes the board to reflect this move if possible
    def makeMove(self, move):
        if not self.movePossible(move):
            print "Move ", move, " is not possible. Move was not executed."
            return

        cur = self.empty
        
        if move == "UP":
            mov = {"y": cur["y"]-1, "x": cur["x"]}
        elif move == "DOWN":
            mov = {"y": cur["y"]+1, "x": cur["x"]}
        elif move == "LEFT":
            mov = {"y": cur["y"], "x": cur["x"]-1}
        elif move == "RIGHT":
            mov = {"y": cur["y"], "x": cur["x"]+1}

        temp = self.board[cur["y"]][cur["x"]]
        self.board[cur["y"]][cur["x"]] = self.board[mov["y"]][mov["x"]]
        self.board[mov["y"]][mov["x"]] = temp
        self.empty["y"] = mov["y"]
        self.empty["x"] = mov["x"]
        
# End of Board Class definition



def main():
    # Create the board and then verify its correctness
    if len(sys.argv) != 1:
        print "Command: python assignment1_p1.py < fileName.txt"
        return

    #initialize board here
    # Pulls from Standard Input to create the Board
    board = [] # Representation of the Board
    empty = {} # Represents the point on the board with the empty space
    #depth limit need to add
    boards_visited = {} # List of previously visited boards   
    steps_til_state=""
    lines = sys.stdin.readlines()

    for line in lines:
        vector = [int(x) for x in line.split(",")]
        board.append(vector)

    for y in range(0, len(board)):
        for x in range(0, len(board[y])):
             #board[y][x] = int(board[y][x])
             if board[y][x] == 0:
                 empty = {"y": y, "x": x}

    #use a queue for breadth first search. This is because we itteratively go by
    #level
    board_obj = Board(board, empty,steps_til_state)
    boards_queue = Queue.Queue()
    boards_queue.put(board_obj)
    found_flag = False
    
    max_size = 0
    visited_nodes = 0
    finalBoard = None
    while boards_queue.empty() == False:
        #keep track of size
        if boards_queue.qsize > max_size:
            max_size = boards_queue.qsize()
        #we add the board state when we pop it off
        tempboard = boards_queue.get()
        #if we found a solution, we're good
        if tempboard.verify() == True:
            found_flag = True
            finalBoard = tempboard
            break
        #explore all our options. if its a valid move and we have never seen it before
        #we add it to the queue
        for move in Board.movesOptions:
            if tempboard.movePossible(move) == True and not tempboard.moveRevisited(move, boards_visited):
                newTempBorad = deepcopy(tempboard)
                newTempBorad.makeMove(move)
                newTempBorad.steps_til_state += move[0]
                boards_queue.put(newTempBorad)
                boards_visited[str(tempboard)] = 1
                visited_nodes += 1
       

    if found_flag == True:
      #  print finalBoard
        print "Moves to Solve: " + finalBoard.steps_til_state
        print "Nodes Visited: " + str(visited_nodes)
        print "Max Queue Size: " + str(max_size)
    else:
        print "UNSOLVABLE"
        print "Nodes Visited: " + str(visited_nodes)
        print "Max Queue Size: " + str(max_size)

   #begin breadth first search

start = time.time()
main()
print "Time Taken: " + str(time.time() - start)
