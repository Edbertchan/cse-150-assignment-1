@echo off
set /p test="Enter TestFile Name: "

echo.
echo Problem1: Board Correctness
py assignment1_p1.py < %test%
echo.
echo Problem2: Breadth First Search
py assignment1_p2.py < %test%
echo.
echo Problem3: Depth First Search
py assignment1_p3.py < %test%
echo.
echo Problem4: Iterative Deepening Search
py assignment1_p4.py < %test%
echo.
echo Problem5: A* Search (Manhattan Heuristic)
py assignment1_p5.py < %test%
pause
