# assignment1_p1.py

__authors__ = "golaes@ucsd.edu, bhaddad@ucsd.edu, eyc010@ucsd.edu"

# import fileinput
import sys

# Start of Board Class definition: Represents the board in the game
class Board:
    board = [] # Representation of the Board
    empty = {} # Represents the point on the board with the empty space

    # Pulls from Standard Input to create the Board
    def __init__(self):
        lines = sys.stdin.readlines()

        for line in lines:
            vector = [int(x) for x in line.split(",")]
            self.board.append(vector)

        for y in range(0, len(self.board)):
            for x in range(0, len(self.board[y])):
                self.board[y][x] = int(self.board[y][x])
                if self.board[y][x] == 0:
                    self.empty = {"y": y, "x": x}

    # Sets printing behaviour of the Board
    def __str__(self):
        string = str(self.board[0])
        for y in range(1, len(self.board)):
            string = string + "\n" + str(self.board[y])
        return string

    # Verifies the board and returns True if the board is correct.
    # Returns False if the board is not correct
    def verify(self):
        count = 0
        size = len(self.board)*len(self.board[0])

        for y in range(0, len(self.board)):
            for x in range(0, len(self.board[y])):
                if self.board[y][x] != count:
                    return False
                count += 1

        return (count == size)
    #note: should check if there are multiple of the same #'s'

    # Takes a possible move ("UP", "DOWN", "LEFT", "RIGHT") and
    # returns True if that move is possible with
    # the given configuration
    def movePossible(self, move):
        if move == "UP":
            return self.empty["y"] != 0
        elif move == "DOWN":
            return self.empty["y"] != (len(self.board)-1)
        elif move == "LEFT":
            return self.empty["x"] != 0
        elif move == "RIGHT":
            return self.empty["x"] != (len(self.board[0])-1)
        else:
            return False

    # Takes a move ("UP", "DOWN", "LEFT", "RIGHT") and
    # changes the board to reflect this move if possible
    def makeMove(self, move):
        if not self.movePossible(move):
            print "Move ", move, " is not possible. Move was not executed."
            return

        cur = self.empty
        
        if move == "UP":
            mov = {"y": cur["y"]-1, "x": cur["x"]}
        elif move == "DOWN":
            mov = {"y": cur["y"]+1, "x": cur["x"]}
        elif move == "LEFT":
            mov = {"y": cur["y"], "x": cur["x"]-1}
        elif move == "RIGHT":
            mov = {"y": cur["y"], "x": cur["x"]+1}

        temp = self.board[cur["y"]][cur["x"]]
        self.board[cur["y"]][cur["x"]] = self.board[mov["y"]][mov["x"]]
        self.board[mov["y"]][mov["x"]] = temp
        self.empty["y"] = mov["y"]
        self.empty["x"] = mov["x"]
# End of Board Class definition


def main():
    # Create the board and then verify its correctness
    if len(sys.argv) != 1:
        print "Command: python assignment1_p1.py < fileName.txt"
        return

    board = Board()
    print "Board Correct?: " + str(board.verify())

main()
