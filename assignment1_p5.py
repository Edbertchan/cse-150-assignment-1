#breadth first

#each state, we will move the 0 either up, down, left, or right
#for each, we check if it is possible. if it is, we will add that state to our queue
#else we dont

#if we end our queue without finding it, we return unsolvable. otherwise we solved it

# assignment1_p1.py

__authors__ = "golaes@ucsd.edu, bhaddad@ucsd.edu, eyc010@ucsd.edu"

import time
import sys
from copy import deepcopy

# Start of Board Class definition: Represents the board in the game
class Board():
    board = [] # Representation of the Board
    empty = {} # Represents the point on the board with the empty space
    movesList = "" # Moves sequence to get to this board state from the start state

    depth = 0 # The depth of the tree
    g = 0 # The g value for the heuristic function
    
    movesOptions = ["UP", "DOWN","LEFT", "RIGHT"]
    
    def __init__(self, board, empty, depth, movesList):
 		self.board = board
 		self.empty = empty
 		self.depth = depth
 		self.movesList = movesList
 		self.g = self.depth + self.heuristic()

    # Sets printing behaviour of the Board
    def __str__(self):
        string = str(self.board[0])
        for y in range(1, len(self.board)):
            string = string + "\n" + str(self.board[y])
        return string

    # Creates a deep copy of the board
    def __deepcopy__(self, memo):
    	return Board(deepcopy(self.board), deepcopy(self.empty), deepcopy(self.depth), deepcopy(self.movesList))

    # Determines two boards are equal if they have the same board configuration
    def __eq__(self, other):
        if isinstance(other, self.__class__):
            for y in range(0, len(self.board)):
                for x in range(0, len(self.board)):
                    if self.board[y][x] != other.board[y][x]:
                        return False
            return True
        else:
            return False
        
    # Determines two boards are not equal if they have different board configurations
    def __ne__(self, other):
        return not self.__eq__(other)

    # Verifies the board and returns True if the board is correct.
    # Returns False if the board is not correct
    def verify(self):
        count = 0
        size = len(self.board)*len(self.board[0])

        for y in range(0, len(self.board)):
            for x in range(0, len(self.board[y])):
                if self.board[y][x] != count:
                    return False
                count += 1

        return (count == size)

    # Takes a possible move ("UP", "DOWN", "LEFT", "RIGHT") and
    # returns True if that move is possible with
    # the given configuration
    def movePossible(self, move):
        if move == "UP":
            return self.empty["y"] != 0
        elif move == "DOWN":
            return self.empty["y"] != (len(self.board)-1)
        elif move == "LEFT":
            return self.empty["x"] != 0
        elif move == "RIGHT":
            return self.empty["x"] != (len(self.board[0])-1)
        else:
            return False

    # Returns True if a board created by the given move has
    # been visited before (as determined by giving a list of
    # previously visited boards
    def moveRevisited(self, move, boards_visited):       
        temp = deepcopy(self)
        temp.makeMove(move)

        return boards_visited.has_key(str(temp))
        #return len([b for b in boards_visited if temp == b]) > 0

    # Takes a move ("UP", "DOWN", "LEFT", "RIGHT") and
    # changes the board to reflect this move if possible
    def makeMove(self, move):
        if not self.movePossible(move):
            print "Move ", move, " is not possible. Move was not executed."
            return

        cur = self.empty
        
        if move == "UP":
            mov = {"y": cur["y"]-1, "x": cur["x"]}
        elif move == "DOWN":
            mov = {"y": cur["y"]+1, "x": cur["x"]}
        elif move == "LEFT":
            mov = {"y": cur["y"], "x": cur["x"]-1}
        elif move == "RIGHT":
            mov = {"y": cur["y"], "x": cur["x"]+1}

        # Adds move to the move list for stat tracking
        self.movesList += move[0]

        temp = self.board[cur["y"]][cur["x"]]
        self.board[cur["y"]][cur["x"]] = self.board[mov["y"]][mov["x"]]
        self.board[mov["y"]][mov["x"]] = temp
        self.empty["y"] = mov["y"]
        self.empty["x"] = mov["x"]

    # Finds the location of a specific number on the board
    # and returns that location as a point in the form [y,x]
    def find(self, num):
        for y in range(0, len(self.board)):
            for x in range(0, len(self.board[0])):
                if self.board[y][x] == num:
                    return [y,x];
        return -1

    # Returns the manhattan distance between two points in the form [y,x]
    def distance(self, p1, p2):
        return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1])

    # Returns the manhattan distance heuristic of the entire board
    # to the correct board configuration as it is
    def heuristic(self):
        ans = 0
        num = 0
        
        if not self.verify():
            for y in range(0, len(self.board)):
                for x in range(0, len(self.board[0])):
                    if num > 0:
                        ans += self.distance([y,x], self.find(num))
                    num += 1
                    
        return ans
        
# End of Board Class definition

globalCount = 0 # Global Count Variable
def boardCompare(board):
    last = board.movesList[-1]

    global globalCount
    globalCount += 1
        
    return (board.g, globalCount)

def main():
    # Create the board and then verify its correctness
    if len(sys.argv) != 1:
        print "Command: python assignment1_p1.py < fileName.txt"
        return

    #initialize board here
    # Pulls from Standard Input to create the Board
    board = [] # Representation of the Board
    empty = {} # Represents the point on the board with the empty space
    depth=0
    lines = sys.stdin.readlines()

    for line in lines:
        vector = [int(x) for x in line.split(",")]
        board.append(vector)

    for y in range(0, len(board)):
        for x in range(0, len(board[y])):
             #board[y][x] = int(board[y][x])
             if board[y][x] == 0:
                 empty = {"y": y, "x": x}

    # Create the initial board object
    board_obj = Board(board, empty, depth, "")

    boards_visited = {} # List of previously visited boards   
    boards_stack = [] # "Stack" (priority queue) that corresponds to the frontier

    # Add the initial board state to both lists
    boards_stack.append(board_obj)
    boards_visited[str(board_obj)] = 1
    found_flag = False

    max_size = 0
    visited_nodes = 0
    finalBoard = None
    while (boards_stack == []) == False: # While the frontier has states
        # Update max_size of stack if necessary
        if len(boards_stack) > max_size:
            max_size = len(boards_stack)

        # Pops a state from the stack and checks to see if it's a goal state
        tempboard = boards_stack.pop()

        # Test Output
        '''
        print tempboard
        print "Moves: " + str(tempboard.movesList)
        print "Moves Made: " + str(tempboard.depth)
        print "Heuristic: " + str(tempboard.g - tempboard.depth)
        print "g: " + str(tempboard.g)
        print "####################"
        '''
        
        if tempboard.verify() == True:
            found_flag = True
            finalBoard = tempboard
            break

        # If not, adds its moves (if they haven't been visited before) to the stack
        for move in Board.movesOptions:
            if tempboard.movePossible(move) == True and not tempboard.moveRevisited(move, boards_visited):
                newTempBorad = deepcopy(tempboard)
                newTempBorad.makeMove(move)
                newTempBorad.depth += 1
                newTempBorad.g = newTempBorad.depth + newTempBorad.heuristic()
                boards_stack.append(newTempBorad)
                boards_visited[str(newTempBorad)] = 1

        visited_nodes += 1
        boards_stack = sorted(boards_stack, key=boardCompare, reverse=True)

    # Once search is complete, output the result
    if found_flag == True:
    	print "Moves to Solve: " + finalBoard.movesList
        print "Nodes Visited: " + str(visited_nodes)
        print "Max Queue Size: " + str(max_size)
    else:
    	print "UNSOLVABLE"
    	print "Nodes Visited: " + str(visited_nodes)
        print "Max Queue Size: " + str(max_size)

   #begin breadth first search

start = time.time()
main()
print "Time Taken: " + str(time.time() - start)
